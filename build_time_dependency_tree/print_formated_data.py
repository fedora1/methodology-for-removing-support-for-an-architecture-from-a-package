#!/usr/bin/python3

# ========================
#
# Script description:
#   This script goes through files in a directory, where each file is named "<repo>@<RPM symbol>",
#   end each line contains package that depends on that symbol.
#
#   This script then prints formated output in form of a table:
#     <repo> | <symbol> | <package> | <status> | <build ID>
#   where <build ID> is the ID of the build to which the <package> belongs and
#   <status> marks whether all resulting artifatcs of the build is 'noarch' or not
#
# Notes:
#   This script utilizes Fedora KOJI API
#   Authentication not required, as all of those are publicly readable information
#
# Authors:
#   Michal Schorm
#
# ========================

import os
import koji
from prettytable import PrettyTable

# Create a Koji session
kojisession_fedora = koji.ClientSession('https://koji.fedoraproject.org/kojihub')
kojisession_rpmfusion = koji.ClientSession('https://koji.rpmfusion.org/kojihub')

# Directory path containing files
directory_path = './../rpm_symbols/isolate_meaningful_results.sh-RESULTS-BUILD-TIME/'

# Function to check if all RPMs for a package are noarch
def are_all_rpm_noarch(repo, build_id):
    # Get RPM information for the build
    if repo == 'rpmfusion-free' or repo == 'rpmfusion-nonfree':
        rpms = kojisession_rpmfusion.listRPMs(buildID=build_id)
    else:
        rpms = kojisession_fedora.listRPMs(buildID=build_id)
    # Check if all RPMs are noarch
    package_name = ''
    status = True
    for rpm in rpms:
        #DEBUG: #print(rpm['name'], rpm['arch'])
        if rpm['arch'] == 'src' :
           package_name = rpm['name']
        if rpm['arch'] != 'noarch' and rpm['arch'] != 'src' :
            status = False
    return status, build_id, package_name

# Function to get the build ID for a specific source RPM
def get_build_id(source_rpm_name):
    # Query Koji to get build ID for the source RPM in Rawhide
    nvr = source_rpm_name[:-4]
    if repo == 'rpmfusion-free' or repo == 'rpmfusion-nonfree':
        return kojisession_rpmfusion.findBuildID(nvr)
    else:
        return kojisession_fedora.findBuildID(nvr)

# Create a table to store the data
table = PrettyTable(['Repository', 'RPM symbol', 'Name of the dependent package', 'Status', 'Build ID'])

# Iterate through files in the directory
for file_name in os.listdir(directory_path):
    # Check if the file is a regular file and not a directory
    if os.path.isfile(os.path.join(directory_path, file_name)):
        # Parse <repo> and <symbol> from the file name
        repo, symbol = file_name.split('@')
        
        # Read all lines from the file
        with open(os.path.join(directory_path, file_name), 'r') as file:
            lines = file.readlines()
        
        # Iterate through all lines in the file
        for line in lines:
            # Strip whitespace from each line
            line = line.strip()
            
            # Get the build ID for the source RPM
            build_id = get_build_id(line)
            
            # Check if build ID is found
            if build_id is not None:
                # Check if all RPMs for the Fedora source package are noarch
                is_noarch, _, package_name = are_all_rpm_noarch(repo, build_id)
                status = 'noarch' if is_noarch else 'arch'
                table.add_row([repo, symbol, package_name, status, build_id])
            else:
                # Print a message if the build ID is not found
                table.add_row([repo, symbol, line, 'N/A (Build ID not found)', 'N/A'])

# Set the alignment of the table columns
table.align['Repository'] = 'l'
table.align['RPM symbol'] = 'l'
table.align['Name of the dependent package'] = 'l'
table.align['Status'] = 'l'
table.align['Build ID'] = 'l'

# Print the table
print(table)
