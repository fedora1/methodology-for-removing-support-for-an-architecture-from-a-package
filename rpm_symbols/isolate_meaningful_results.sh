#!/bin/bash

# =============
#
# Script description:
#   For each file in source directory, if the file is NOT empty, copy the file into a results directory,
#   print file name, file content and a new line
#
# Authors:
#   Michal Schorm
#
# ==============

SOURCE_DIR_BUILD_TIME="./get_dependency_tree.sh-RESULTS-BUILD-TIME/"
SOURCE_DIR_RUN_TIME="./get_dependency_tree.sh-RESULTS-RUN-TIME/"
RESULTS_DIR_BUILD_TIME="./isolate_meaningful_results.sh-RESULTS-BUILD-TIME/"
RESULTS_DIR_RUN_TIME="./isolate_meaningful_results.sh-RESULTS-RUN-TIME/"

mkdir -p "$RESULTS_DIR_BUILD_TIME" "$RESULTS_DIR_RUN_TIME"

echo -e "\n == ISOLATING BUILD TIME DEPENDENCIES == \n"
for file in "$SOURCE_DIR_BUILD_TIME"/*; do
    # Check if the file is not empty
    if [ -s "$file" ]; then
        cp -a "$file" "$RESULTS_DIR_BUILD_TIME"
        # Print file name
        echo "FILE: $(basename $file) isolated"
        # Print file content
        cat "$file"
        # Print an empty line
        echo
    fi
done

echo -e "\n == ISOLATING RUN TIME DEPENDENCIES == \n"
for file in "$SOURCE_DIR_RUN_TIME"/*; do
    # Check if the file is not empty
    if [ -s "$file" ]; then
        cp -a "$file" "$RESULTS_DIR_RUN_TIME"
        # Print file name
        echo "FILE: $(basename $file) isolated"
        # Print file content
        cat "$file"
        # Print an empty line
        echo
    fi
done

# Workaround for bug: https://bugzilla.redhat.com/show_bug.cgi?id=2246075
#  ditch the Epoch part of the string, if it's 0
for file in "$RESULTS_DIR_BUILD_TIME"/* "$RESULTS_DIR_RUN_TIME"/* ; do
    # Use sed to remove lines containing "-0:" and save the changes inline
    sed -i 's/-0:/-/g' "$file"
done
