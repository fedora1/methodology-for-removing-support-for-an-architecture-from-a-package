#!/usr/bin/python3

# ========================
#
# Script description:
#   This script will fetch you all symbols provided by all sub-packages of a packages from a list,
#   from the latest build on selected Fedora release,
#   writes the results to STDOUT, write the pure results to a file, and finally sorts the file leaving only unique entries
#
# Notes:
#   This script utilizes Fedora KOJI API
#   Authentication not required, as all of those are publicly readable information
#
# Authors:
#   Michal Schorm
#
# ========================


# Creating directory for result files
import os

# https://koji.fedoraproject.org/koji/api
import koji

# Koji hub URL
KOJI_HUB = 'https://koji.fedoraproject.org/kojihub'
# KOJI tag we want to search in
FEDORA_RELEASE = 'rawhide'
# List of package names to process
PACKAGE_NAMES = ['mariadb', 'community-mysql']
# Results file name
RESULTS_FILE_NAME = 'results'
# Sorted, unique results
SORTED_RESULTS_FILE_NAME = 'results_sorted'
# Results directory
RESULTS_DIRECTORY = "./get_rpm_symbols.py-RESULTS/"
if not os.path.exists(RESULTS_DIRECTORY):
    os.makedirs(RESULTS_DIRECTORY)


# Create a Koji session
kojisession = koji.ClientSession(KOJI_HUB)

# Function to get the latest production build for a package in Fedora Rawhide
def get_latest_fedora_rawhide_build(package_name):
    # Get the latest build for the package in Fedora Rawhide
    latest_builds = kojisession.getLatestBuilds(FEDORA_RELEASE, package=package_name, type='rpm')
    # Return the latest build information
    return latest_builds

# Function to extract and write the PROVIDES section to a file and standard output
def write_provides_to_file(rpm_id, package_name):

    try:
        # Get the RPM name
        rpm_info = kojisession.getRPM(rpm_id)

        rpm_name = rpm_info['name']
        rpm_arch = rpm_info['arch']
        rpm_info_string = f"RPM '{rpm_name}' (Package: {package_name}, Arch: {rpm_arch}, rpmID {rpm_id})"
        
        results_file = open(RESULTS_DIRECTORY + RESULTS_FILE_NAME, 'a')

        # Skip RPMs with "-debuginfo" or "-debugsource" in their name
        if "-debuginfo" not in rpm_name and "-debugsource" not in rpm_name:
            # Get the PROVIDES section for the RPM
            rpm_headers = kojisession.getRPMHeaders(rpm_id)
            provides_section = rpm_headers.get('PROVIDES', [])
            print(f"PROVIDES section for {rpm_info_string}:")
            if provides_section:
                for item in provides_section:
                    print(f"\t{item}")
                    results_file.write(f"{item}\n")
            else:
                print(f"No PROVIDES section found for {rpm_info_string}")
        else:
            print(f"Skipping {rpm_info_string} - Contains debuginfo/debugsource")
    except Exception as e:
        print(f"Error fetching RPM headers: {e}")

try:
    for package_name in PACKAGE_NAMES:
        latest_builds = get_latest_fedora_rawhide_build(package_name)
        if latest_builds:
            latest_build = latest_builds[0]  # Get the latest build from the list
            build_id = latest_build['build_id']
            print(f"\nLatest production build for '{package_name}' in Fedora Rawhide:")
            print(f"Build ID: {build_id}")
            
            # List all RPMs in the build and extract PROVIDES section for each RPM (skipping debuginfo/debugsource RPMs)
            rpms = kojisession.listRPMs(buildID=build_id)
            for rpm in rpms:
                rpm_id = rpm['id']
                # Extract and write the PROVIDES section to the file and standard output for the current RPM
                write_provides_to_file(rpm_id, package_name)
        else:
            print(f"No production builds found for '{package_name}' in Fedora Rawhide.")
    print(f"Results written to '{RESULTS_DIRECTORY+RESULTS_FILE_NAME}'")
except Exception as e:
    print(f"Error: {e}")


# ============================
# Sort the file with results, leave only unique entries

# Read data from the input file
with open(RESULTS_DIRECTORY+RESULTS_FILE_NAME, 'r') as file:
    data = file.read().splitlines()
# Perform unique sort
unique_sorted_data = sorted(set(data))
# Write sorted unique values to the output file
with open(RESULTS_DIRECTORY+SORTED_RESULTS_FILE_NAME, 'w') as file:
    for item in unique_sorted_data:
        file.write("%s\n" % item)
print(f"Unique sorted data has been written to {RESULTS_DIRECTORY+SORTED_RESULTS_FILE_NAME}")
