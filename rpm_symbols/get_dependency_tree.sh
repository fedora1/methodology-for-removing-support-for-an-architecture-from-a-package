#!/bin/bash

# =======================
#
# Script description:
#   Runs on top of a file which contain RPM provides to be checked. Each provide on separate line.
#   Runs separate DNF Repoquery calls to find build-time and run-time packages that depend on listed RPM provides
#   Runs on top of several repositories
#   Returns two separate directories - for build-time and for run-time results
#     Directories contains files for every combination (repository * RPM provide)
#     Each file contains result of the DNF repoquery call
#     File can be empty, when no package depend on given provide in given repository
#       That can be useful for finding useful data (= just ignore empty files)
#
# Authors:
#   Michal Schorm
#
# ========================

RESULTS_DIRECTORY="get_dependency_tree.sh-RESULTS"

mkdir -p "$RESULTS_DIRECTORY-"{"BUILD-TIME","RUN-TIME"}

for REPO in "rawhide" "rpmfusion-free" "rpmfusion-nonfree" ; do
  for PROVIDE in $(<get_rpm_symbols.py-RESULTS/results_sorted) ; do
    echo -e "\n\nREPO: $REPO ; PROVIDE: $PROVIDE";
    dnf -q --repo="$REPO"-source repoquery --whatrequires "$PROVIDE" --alldeps | tee "./$RESULTS_DIRECTORY-BUILD-TIME/$REPO@$PROVIDE"
    dnf -q --repo="$REPO" repoquery --whatrequires "$PROVIDE" --alldeps | tee "./$RESULTS_DIRECTORY-RUN-TIME/$REPO@$PROVIDE"
  done
done

